/*
    /hfp/journey/bus/1219/4614/1/XXX/2110/1250203/60;24/29/14/53    
        VP  
        desi    "4614"
        dir "1"
        oper    "XXX"
        veh "1219"
        tst "2017-06-04T18:27:34.651Z"
        tsi 1496600854
        spd 2.22
        lat 60.21557
        long    24.94327
        dl  212
        oday    "XXX"
        jrn "XXX"
        line    "4614"
        start   "2110"
*/

class Journey {
    constructor(desi, dir, oper, veh, tst, tsi, spd, lat, lng, dl, oday, jrn, line, start, hdg) {
        this._desi = desi;
        this._dir = dir;
        this._oper = oper;
        this._veh = veh;
        this._tst = tst;
        this._spd = spd;
        this._lat = lat;
        this._lng = lng;
        this._dl = dl;
        this._oday = oday;
        this._jrn = jrn;
        this._line = line;
        this._start = start;
        this._hdg = hdg;
    }

    static fromJSON(json) {
        const vp = json.VP;
        const journey = new this(vp.desi, vp.dir, vp.oper, vp.veh, vp.tst, vp.tsi, vp.spd, vp.lat, vp.long, vp.dl, vp.oday, vp.jrn, vp.line, vp.start, vp.hdg);
        return journey;
    }

    getMarkerData() {
        let marker = {};
        const position = L.latLng(this._lat, this._lng);
        marker["position"] = position;

        const icon = this.getVehicleIcon();
        marker["icon"] = icon;

        return marker;
    }

    interpretJore(line) {

        // 'JORE' code handling
        // https://github.com/HSLdevcom/navigator-proto/blob/master/src/routing.coffee#L40
        let route = line;
        if (line.match(/^1019/) !== null) {
            // XXX: Ferry to do
            route = '%';
        } else if (line.match(/^1300/) !== null) {
            // subway
            route = line.substring(4,5);
        } else if (line.match(/^300/) !== null) {
            // train
            route = line.substring(4,5);
        } else if (line.match(/^10(0|10)/) !== null) {
            route = line.replace(/^.0*/, "");
        } else if (line.match(/^(1|2|4).../) !== null) {
            route = line.replace(/^.0*/, "");
        }
        return route;
    }

    getVehicleIcon() {

        const vehicleType = this.getVehicleType();
        const classes = 'vehicleMarker' + ' ' + vehicleType;
        const iconHtml = this.getVehicleIconHtml();
        const icon = L.divIcon({
            html: iconHtml,
            iconSize: [30, 30],
            className: classes,
        });

        return icon;
    }

    getVehicleIconHtml() {
        const line = this._desi ? this.interpretJore(this._desi) : "?";

        if (this._hdg !== undefined) {
            const heading = this._hdg + 90;
            const textSpan = '<span style="display: block; transform:rotateZ(' + (-heading) + 'deg);">' + line + '</span>';
            const iconHtml = '<div id="' + this._veh + '" style="display: block; transform:rotateZ(' + heading + 'deg);">' + textSpan + '</div>';
            return iconHtml;
        } else {
            const textSpan = '<span>' + line + '</span>';
            const iconHtml = '<div id="' + this._veh + '">' + textSpan + '</div>';
            return iconHtml;
        }
    }

    getVehicleType() {

        const vehicleId = this._veh;

        if (/^R/.test(vehicleId)) {
            return 'Tram';
        } else if (/^\d/.test(vehicleId)) {
            return 'Bus';
        } else if (/^H/.test(vehicleId)) {
            return 'Train';
        } else if (/^metro/.test(vehicleId)) {
            return 'Metro';
        } else if (/^1019/.test(vehicleId)) {
            return 'Ferry';
        } else {
            return 'Bus';
        }
    }

    hasHeading() {
        return this._hdg !== undefined;
    }

    setHeading(heading) {
        this._hdg = heading;
    }
}

