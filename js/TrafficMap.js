class TrafficMap {


    constructor() {
        this._BUFFER_TIME = 500;
        this._MQTT_HOST = '213.138.147.225';
        this._MQTT_TOPIC = '/hfp/journey/#';

        this._vehicleMarkers = new Map();
    }

    create(containerId = 'map') {
        const map = L.map(containerId).setView([60.197, 24.922], 15);

        this._map = map;

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
    }

    run() {

        // connect to hsl mqtt server
        const client = mqtt.connect('mqtt://' + this._MQTT_HOST + ":1883");

        // when connected, subscribe to topic
        client.on('connect', () => {
            client.subscribe(this._MQTT_TOPIC);
        });

        // make on Observable out of mqtt client's onmessage event
        const messages = Rx.Observable.fromEvent(
            client,
            'message',
            (topic, message) => { return { "topic": topic, "message": message } }
        );

        // group messages by vehicle id for BUFFER_TIME milliseconds
        const grouped = messages.groupByUntil(
            m => { let json = JSON.parse(m.message); return json.VP.veh },
            m => m,
            () => Rx.Observable.interval(this._BUFFER_TIME)
        );

        grouped
            //.take(1)
            .subscribe(group => {

                // find the entry it with latest timestamp
                group.max(j => j._tsi).subscribe(m => {

                    // lay it out on map
                    const json = JSON.parse(m.message);

                    const key = json.VP.veh;

                    const journey = Journey.fromJSON(JSON.parse(m.message));

                    // is this vechile already on map?
                    if (this._vehicleMarkers.has(key)) {
                        const previousMarker = this._vehicleMarkers.get(key);

                        // do we know where the vehicle is heading?
                        if (!journey.hasHeading()) {
                            // if not, calculate heading using previous marker
                            const prevLatLng = previousMarker.getLatLng();
                            const currentLatLng = L.latLng(json.VP.lat, json.VP.long);

                            // if vehicle has moved, update the heading
                            if (prevLatLng != currentLatLng) {
                                const heading = L.GeometryUtil.bearing(prevLatLng, currentLatLng);
                                journey.setHeading(heading);
                            }
                        }

                        // remove previous marker
                        this._map.removeLayer(previousMarker);
                    }

                    const markerData = journey.getMarkerData();

                    const marker = L.marker(markerData.position);
                    marker.setIcon(markerData.icon);

                    marker.bindPopup('<pre>' + JSON.stringify(json, null, 2) + '</pre>');

                    marker.addTo(this._map);

                    this._vehicleMarkers.set(key, marker);
                });
        });
    }
}
